unit GridForm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects, Radiant.Shapes,
  FMX.Layouts, System.Generics.Collections, MineSweeperEngine,
  MineSweeperNeighbourCalculator, FMX.Edit, FMX.EditBox, FMX.SpinBox,
  MinesweeperAudioManagerSound, MineSweeperFMSettings, MineSweeperSettings,
  MineSweeperDataModuleFMX;

type
  TRadiantHexCell = class(TRadiantHexagon)
  public
    col, row : integer;
    aLabel : TLabel;
    procedure SetSizeAndPosition(Side : single);
    constructor Create(AOwner: TComponent); override;
  end;

  TShapeList = TList<TRadiantHexCell>;
  TShapeGrid = array of array of TRadiantHexCell;

  TForm23 = class(TForm)
    RadiantHexagon1: TRadiantHexagon;
    btnNewGame: TButton;
    lGameState: TLabel;
    lMines: TLabel;
    Layout2: TLayout;
    Layout3: TLayout;
    sbFlag: TSpeedButton;
    btnSettings: TButton;
    lTimer: TLabel;
    Timer1: TTimer;
    procedure RadiantHexagon1MouseEnter(Sender: TObject);
    procedure RadiantHexagon1MouseLeave(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnNewGameClick(Sender: TObject);
    procedure RadiantHexagon1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure btnSettingsClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RadiantHexagon1Paint(Sender: TObject; Canvas: TCanvas;
      const ARect: TRectF);
    procedure OverpaintHexagon(Sender: TObject; Canvas: TCanvas;
      const ARect: TRectF);
    procedure Timer1Timer(Sender: TObject);
  private
    ms : TMineSweeperEngine;
    Shapes : TShapeList;
    Grid : TShapeGrid;
    GameLayout : TLayout;
    GameSettings : TGameSettings;
    function HexagonSideLength : single;
    procedure DrawShapes;
    procedure UpdateShape(h : TRadiantHexCell);
    procedure UpdateGameState;
    procedure UpdateGrid;
    procedure NewGame;
    procedure SetHexagonSizeAndPositions;
  public
    { Public declarations }
    function RowCount : integer;
    function ColCount : integer;
    function MineCount : integer;
  end;

var
  Form23: TForm23;

implementation

uses
  System.Rtti,
  System.Math;

{$R *.fmx}

procedure TForm23.btnNewGameClick(Sender: TObject);
begin
  NewGame;
end;

procedure TForm23.btnSettingsClick(Sender: TObject);
begin
  TfSettingsFM.GetGameSettings(GameSettings, NewGame);
end;


function TForm23.ColCount: integer;
begin
  result := GameSettings.Width;
end;

procedure TForm23.DrawShapes;
begin
  SetLength(Grid, ColCount, RowCount);
  Shapes.Clear;

  if GameLayout <> nil then //first game
    GameLayout.Free;

  GameLayout := TLayout.Create(Self);
  GameLayout.Parent := Self;
  GameLayout.Align := TAlignLayout.Client;

  //somehow decide to layout horizontally or vertically

  for var I := 0 to ColCount - 1 do
  begin
    for var j := 0 to RowCount - 1 do
    begin
      var h := TRadiantHexCell.Create(GameLayout);
      h.Parent := GameLayout;

      h.col := i;
      h.row := j;

      h.OnMouseUp := RadiantHexagon1MouseUp;
      h.OnPaint := OverpaintHexagon;

      Shapes.Add(h);
      Grid[i, j] := h;
    end;
  end;
  SetHexagonSizeAndPositions;
end;

procedure TForm23.FormCreate(Sender: TObject);
begin
  GameSettings := TGameSettings.Create;
  RadiantHexagon1.Visible := False;
  Shapes := TShapeList.Create;
  ms := TMineSweeperEngine.Create;
  ms.Sound := TMineSweeperAudioManagerSound.Create;

  GameSettings.Width := 9;
  GameSettings.height := 9;
  GameSettings.mines := 10;

  NewGame;
end;

procedure TForm23.FormDestroy(Sender: TObject);
begin
  Shapes.Free;
  GameSettings.Free;
end;

procedure TForm23.FormResize(Sender: TObject);
begin
  SetHexagonSizeAndPositions;
end;

function TForm23.HexagonSideLength: single;
begin
  if not Assigned(GameLayout) then
    Exit(1);

  var MaxSideVertical := GameLayout.Height / (RowCount + 0.5) / 1.7321;
  var MaxSideHorizontal := GameLayout.Width / (ColCount + 0.5) / 1.5;
  Result := Min(MaxSideVertical, MaxSideHorizontal);
end;

function TForm23.MineCount: integer;
begin
  result := GameSettings.Mines
end;

procedure TForm23.NewGame;
begin
  ms.InitialiseBoard(ColCount, RowCount, MineCount);
  ms.NeighbourCalculator := THexagonNeighbourCalculator.Create(ColCount, RowCount);
  DrawShapes;
  UpdateGrid;
end;

procedure TForm23.OverpaintHexagon(Sender: TObject; Canvas: TCanvas;
  const ARect: TRectF);
begin
  var h := (Sender as TRadiantHexCell);
  var cell := ms.Board[h.col, h.row];

  ARect.Inflate(-(ARect.Width) * 0.15, -(ARect.Height) * 0.15); //reduce the rect size so image fits within hexagon.

  if cell.Visible and cell.Mine then
  begin
    var b := dmMineSweeper.Images.Bitmap(TSizeF.Create(ARect.Width, ARect.Height), 1);
    Canvas.DrawBitmap(b, Rect(0,0, b.Width, b.Height), ARect, 1);
  end;
  if cell.Flag then
  begin
    var b := dmMineSweeper.Images.Bitmap(TSizeF.Create(ARect.Width, ARect.Height), 0);
    Canvas.DrawBitmap(b, Rect(0,0, b.Width, b.Height), ARect, 1);
  end;
end;

procedure TForm23.RadiantHexagon1MouseEnter(Sender: TObject);
begin
  (Sender as TRadiantHexagon).Fill.Color := TAlphaColorRec.Blue;
end;

procedure TForm23.RadiantHexagon1MouseLeave(Sender: TObject);
begin
  (Sender as TRadiantHexagon).Fill.Color := $FFE0E0E0;
end;

procedure TForm23.RadiantHexagon1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  if not (Sender is TRadiantHexCell) then
    Exit;

  var h := Sender as TRadiantHexCell;

  if Button = TMouseButton.mbLeft then
  begin
    if ms.Board[h.col,h.row].Visible then
      Button := TMouseButton.mbMiddle
    else
    if sbFlag.IsPressed then
      Button := TMouseButton.mbRight;
  end;


  case Button of
    TMouseButton.mbLeft: ms.LeftClick(h.col, h.row);
    TMouseButton.mbRight: ms.RightClick(h.col, h.row);
    TMouseButton.mbMiddle: ms.MiddleClick(h.col, h.row);
  end;
  if button = TMouseButton.mbRight then
    UpdateShape(h)
  else
    UpdateGrid;

  UpdateGameState;
end;

procedure TForm23.RadiantHexagon1Paint(Sender: TObject; Canvas: TCanvas;
  const ARect: TRectF);
begin
  var b := dmMineSweeper.Images.Bitmap(TSizeF.Create(ARect.Width, ARect.Height), 0);
  Canvas.DrawBitmap(b, Rect(0,0, b.Width, b.Height), ARect, 1);
end;

function TForm23.RowCount: integer;
begin
  result := GameSettings.Height;
end;

procedure TForm23.SetHexagonSizeAndPositions;
begin
  var Side := HexagonSideLength;
  for var s in Shapes do
    s.SetSizeAndPosition(Side);
end;

procedure TForm23.Timer1Timer(Sender: TObject);
begin
  lTimer.Text := ms.RunningTime.Elapsed.Seconds.ToString;
end;

procedure TForm23.UpdateGameState;
begin
  lGameState.Text := TRttiEnumerationType.GetName<TGameState>(ms.GameState);
  lMines.Text := (ms.TotalMineCount - ms.FlagsPlaced).ToString;
end;

procedure TForm23.UpdateGrid;
begin
  for var s in Shapes do
    UpdateShape(s);
  UpdateGameState;
end;

procedure TForm23.UpdateShape(h: TRadiantHexCell);
begin
  var cell := ms.Board[h.col, h.row];
  if cell.Flag then
  begin
//    h.aLabel.Text := 'Flag';
      h.aLabel.Text := '';
    h.Fill.Color := TAlphaColorRec.Skyblue;
  end
  else
  if cell.Visible then
  begin
    if cell.Mine then
    begin
//      h.aLabel.Text := 'Mine';
      h.aLabel.Text := '';
      h.Fill.Color := TAlphaColorRec.Red;
    end
    else
    begin
      h.aLabel.Text := cell.NeighbourCount.ToString;
      h.Fill.Color := TAlphaColorRec.Beige;
    end;
  end
  else
  begin
//    h.aLabel.Text := '?';
    h.aLabel.Text := '';
    h.Fill.Color := TAlphaColorRec.Lightgray;
  end;
end;

{ TRadiantHexCell }

constructor TRadiantHexCell.Create(AOwner: TComponent);
begin
  inherited;
  aLabel := TLabel.Create(Self);
  aLabel.Parent := Self;
  aLabel.Align := TAlignLayout.Client;
  aLabel.TextSettings.HorzAlign := TTextAlign.Center;
  aLabel.StyledSettings := aLabel.StyledSettings - [TStyledSetting.Size];
end;

procedure TRadiantHexCell.SetSizeAndPosition(Side: single);
begin
  Width := side * 2;
    Height := side * 2 * 0.87;
//  Height := side * 1.7321;
  Position.X := col * Width * 0.75;
  Position.Y := Height * (row + (0.5 * (col mod 2)));
  aLabel.Font.Size := Height / 3;
end;

end.
