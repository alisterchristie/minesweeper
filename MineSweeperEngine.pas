unit MineSweeperEngine;

interface

uses
  System.Generics.Collections,
  MineSweeperNeighbourCalculator,
  MinesweeperSoundInterface,
  System.Diagnostics;

type
  TMineSweeperCell = record
  public
    Mine : boolean;
    Flag : boolean;
    Visible : boolean;
    NeighbourCount : integer;
    X, Y : integer;
    procedure Clear;
  end;

  TBoard = Array of Array of TMineSweeperCell;

  TCellProc = reference to procedure(var Cell : TMineSweeperCell);

  TGameState = (Won, Lost, Playing);

  TMineSweeperEngine = class
  private
    FFlagsPlaced : integer;
    FMines : integer;
    FirstMove : boolean;
    procedure CheckValid(x: Integer; y: Integer);
    procedure CheckWinState;
  protected
    procedure CalculateAllNeighbours;
    procedure CalculateNeighbours(var Cell : TMineSweeperCell);
    procedure ForEveryCell(CellProc : TCellProc);

    function GetColCount : integer;
    function GetRowCount : integer;

    function GetNeighbours (x, y : integer): TCellRefs;
    function ValidCell(x, y : integer) : boolean;
    procedure RevealBoard;
    function FlaggedNeighbourCount(x, y: integer): integer;
    function Show(x, y : integer; CheckWinStateRequired : boolean) : boolean;
    function ShowNeighbours(x, y: integer; CheckWinStateRequired : boolean; CheckIfMineCountIsCorrect : boolean) : boolean;
    procedure PlaceMines(MineCount : integer);
  public
    RunningTime : TStopWatch;
    NeighbourCalculator : INeighbourCalculator;
    Board : TBoard;
    GameState : TGameState;
    Sound : IMineSweeperSound;
    procedure InitialiseBoard(ColCount, RowCount, MineCount  : integer);

    function TotalMineCount : integer;

    function Visible(x, y : integer) : boolean;
    function Flagged(x, y : integer) : boolean;
    function FlagsPlaced : integer;

    function SetFlag(const x, y : integer) : boolean;
    function Width : integer;
    function Height : integer;

    constructor Create;

    //High level methods
    procedure LeftClick(x, y : integer);
    procedure RightClick(x, y : integer);
    procedure MiddleClick(x, y : integer);
  end;

implementation

uses
  System.SysUtils,
  System.Classes;

{ TMineSweeperCell }

procedure TMineSweeperCell.Clear;
begin
  Mine := False;
  Visible := False;
  Flag := False;
  NeighbourCount := -1;
end;

{ TMineSweeperEngine }

procedure TMineSweeperEngine.CalculateAllNeighbours;
begin
  ForEveryCell(CalculateNeighbours);
end;

procedure TMineSweeperEngine.CalculateNeighbours(var Cell: TMineSweeperCell);
var
  Neighbours : TCellRefs;
  MineCount : integer;
  Neighbour: TCellRef;
begin
  if Cell.Mine then
    Exit;

  MineCount := 0;
  Neighbours := GetNeighbours(cell.X, cell.Y);
  for Neighbour in Neighbours do
  begin
    if Board[Neighbour.x, Neighbour.y].Mine then
      inc(MineCount);
  end;

  Cell.NeighbourCount := MineCount;
end;

function TMineSweeperEngine.SetFlag(const x, y : integer) : boolean;
begin
  CheckValid(x,y);
  if not Visible(x, y) then
  begin
    Result := True;
    Board[x,y].Flag := not Board[x,y].Flag;
    CheckWinState;
    if Board[x,y].Flag then
      inc(FFlagsPlaced)
    else
      dec(FFlagsPlaced);
  end else
    Result := false;
end;

function TMineSweeperEngine.Flagged(x, y: integer): boolean;
begin
  CheckValid(x, y);
  result := Board[x,y].Flag;
end;

function TMineSweeperEngine.FlaggedNeighbourCount(x, y: integer): integer;
var
  CellRefs: TCellRefs;
  c: TCellRef;
begin
  result := 0;
  CellRefs := GetNeighbours(x, y);
  for c in CellRefs do
  begin
    if Board[c.x, c.y].Flag then
      inc(Result);
  end;
end;

function TMineSweeperEngine.FlagsPlaced: integer;
begin
  result := FFlagsPlaced;
end;

procedure TMineSweeperEngine.CheckWinState;
var
  StillHasHiddenCells: Boolean;
  VisibleMine : boolean;
  InvalidFlags : Boolean;
begin
  if GameState <> TGameState.Playing then
    Exit;

  StillHasHiddenCells := False;
  InvalidFlags := False;

  ForEveryCell(procedure(var Cell: TMineSweeperCell)
    begin
      if not (Cell.Visible or cell.Flag) then
        StillHasHiddenCells := True;
      if Cell.Visible and Cell.Mine then
        VisibleMine := True;
      if Cell.Flag and (not Cell.Mine) then
        InvalidFlags := True;
    end);
  if VisibleMine then
    GameState := TGameState.Lost
  else if (not StillHasHiddenCells) and (not InvalidFlags) then
    GameState := TGameState.Won;

  if GameState <> TGameState.Playing then
    RunningTime.Stop;
end;

constructor TMineSweeperEngine.Create;
begin
  inherited;
  Sound := TMinesweeperDummySound.Create;
  RunningTime := TStopWatch.Create;
end;

procedure TMineSweeperEngine.CheckValid(x: Integer; y: Integer);
begin
  if not ValidCell(x, y) then
    raise Exception.Create(Format('Invalid Cell [%d, %d]', [x, y]));
end;

procedure TMineSweeperEngine.ForEveryCell(CellProc: TCellProc);
var
  x: Integer;
  y: Integer;
  ColCount, RowCount  : integer;
begin
  ColCount := GetColCount;
  RowCount := GetRowCount;

  for x := 0 to ColCount-1 do
    for y := 0 to RowCount-1 do
      CellProc(Board[x, y]);
end;

function TMineSweeperEngine.GetColCount: integer;
begin
  result := Length(Board);
end;

function TMineSweeperEngine.GetNeighbours(x, y : integer): TCellRefs;
begin
  result := NeighbourCalculator.GetNeighbours(x, y);
end;

function TMineSweeperEngine.GetRowCount: integer;
begin
  result := Length(Board[0]);
end;

function TMineSweeperEngine.Height: integer;
begin
  result := GetRowCount;
end;

procedure TMineSweeperEngine.InitialiseBoard(ColCount, RowCount, MineCount  : integer);
var
  x: Integer;
  y: Integer;
begin
  RunningTime := TStopwatch.Create;
  SetLength(Board, ColCount, RowCount);

  FMines := MineCount;
  ColCount := GetColCount;
  RowCount := GetRowCount;

  for x := 0 to ColCount-1 do
  begin
    for y := 0 to RowCount-1 do
    begin
      Board[x,y].Clear;
      Board[x,y].x := x;
      Board[x,y].y := y;
    end;
  end;

  GameState := Playing;
  NeighbourCalculator := TSquareNeighbourCalculator.Create(ColCount, RowCount);
  FFLagsPlaced := 0;
  FirstMove := True;
end;

procedure TMineSweeperEngine.LeftClick(x, y : integer);
begin
  if Flagged(x, y) then
  begin
    Sound.Error;
    Exit;
  end;

  if Show(x, y, True) then
  begin
    var c := Board[x, y];
    if c.Visible then
    begin
      if c.Mine then
        Sound.Mine
      else
        Sound.Click;
    end;
  end else
    Sound.Error;

  if FirstMove then
  begin
    PlaceMines(FMines);
    if Board[x,y].NeighbourCount = 0 then
      ShowNeighbours(x, y, False, False);
    RunningTime := TStopWatch.StartNew;
  end;

  FirstMove := False;
end;

procedure TMineSweeperEngine.MiddleClick(x, y : integer);
begin
  if not ShowNeighbours(x, y, True, True) then
  begin
    Sound.Error;
    Exit;
  end;
  if GameState = TGameState.Won then
    Sound.Win
  else if GameState = TGameState.Lost then
    Sound.Mine //can't happen on first move
  else
  begin
    Sound.Click;
    TThread.CreateAnonymousThread(
      procedure
      begin
        Sleep(80);
        Sound.Click;
      end
    ).Start;
  end;
  FirstMove := False;
end;

procedure TMineSweeperEngine.PlaceMines(MineCount : integer);
var
  x, y : integer;
  maxiterations : integer;
begin
  maxiterations := MineCount * 100;

  repeat
    x := Random(Length(Board));
    y := Random(Length(Board[0]));
    if (not Board[x,y].Mine) and (not Visible(x, y)) then
    begin
      Board[x,y].Mine := True;
      Dec(MineCount);
    end;

    Dec(maxiterations);
    if maxiterations < 0 then { This is taking way too long! }
      raise Exception.Create('Could not place mines');
  until MineCount = 0;
  CalculateAllNeighbours;
end;

procedure TMineSweeperEngine.RevealBoard;
begin
  ForEveryCell(
    procedure(var Cell : TMineSweeperCell)
    begin
      Cell.Visible := True;
    end
  );
end;

procedure TMineSweeperEngine.RightClick(x, y : integer);
begin
  if SetFlag(x, y) then
  begin
    if GameState = TGameState.Won then
      Sound.Win
    else
      Sound.Flag
  end
  else
    Sound.Error;
  FirstMove := False;
end;

function TMineSweeperEngine.Show(x, y : integer; CheckWinStateRequired : boolean) : boolean;
begin
  CheckValid(x, y);

  if Visible(x, y) then
    Exit(False);

  result := True;
  Board[x,y].Visible := True;

  if Board[x,y].Mine then
  begin
    RevealBoard;
    GameState := TGameState.Lost;
    RunningTime.Stop;
    Exit;
  end;

  if Board[x,y].NeighbourCount = 0 then
    ShowNeighbours(x, y, False, False);

  if CheckWinStateRequired then
    CheckWinState;
end;

function TMineSweeperEngine.ShowNeighbours(x, y: integer; CheckWinStateRequired : boolean; CheckIfMineCountIsCorrect : boolean) : boolean;
var
  Neighbours: TCellRefs;
  N: TCellRef;
begin
  if CheckIfMineCountIsCorrect then
  begin
    if Board[x, y].NeighbourCount <> FlaggedNeighbourCount(x, y) then
      Exit(False);
  end;

  Neighbours := GetNeighbours(x, y);
  for N in Neighbours do
  begin
    if not (Visible(N.x, N.y) or Flagged(N.x, N.y)) then
      Show(N.x, N.y, False);
  end;
  if CheckWinStateRequired then
    CheckWinState;
  result := True;
end;

function TMineSweeperEngine.TotalMineCount: integer;
begin
  result := FMines;
end;

function TMineSweeperEngine.ValidCell(x, y: integer): boolean;
begin
  result := NeighbourCalculator.ValidCell(x,y);
end;

function TMineSweeperEngine.Visible(x, y: integer): boolean;
begin
  CheckValid(x, y);
  result := Board[x, y].Visible;
end;

function TMineSweeperEngine.Width: integer;
begin
  result := GetColCount;
end;

end.
