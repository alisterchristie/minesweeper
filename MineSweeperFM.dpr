program MineSweeperFM;



uses
  System.StartUpCopy,
  FMX.Forms,
  MineSweeperFMForm in 'MineSweeperFMForm.pas' {frmGame},
  MineSweeperEngine in 'MineSweeperEngine.pas',
  MineSweeperSettings in 'MineSweeperSettings.pas',
  MinesweeperSoundInterface in 'MinesweeperSoundInterface.pas',
  MineSweeperFMXSound in 'MineSweeperFMXSound.pas',
  AudioManager in 'AudioManager.pas',
  MinesweeperAudioManagerSound in 'MinesweeperAudioManagerSound.pas',
  MineSweeperFMSettings in 'MineSweeperFMSettings.pas' {fSettingsFM},
  MineSweeperNeighbourCalculator in 'MineSweeperNeighbourCalculator.pas',
  MineSweeperDataModuleFMX in 'MineSweeperDataModuleFMX.pas' {dmMineSweeper: TDataModule};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  Application.CreateForm(TfrmGame, frmGame);
  Application.CreateForm(TdmMineSweeper, dmMineSweeper);
  Application.Run;
end.

