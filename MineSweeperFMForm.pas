unit MineSweeperFMForm;

interface

uses
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Variants,
  FMX.Types,
  FMX.Controls,
  FMX.Forms,
  FMX.Graphics,
  FMX.Dialogs,
  MineSweeperEngine,
  FMX.Objects,
  MineSweeperSettings,
  FMX.StdCtrls,
  FMX.Controls.Presentation,
  MinesweeperSoundInterface, MineSweeperDataModuleFMX;

type
  TfrmGame = class(TForm)
    PaintBox1: TPaintBox;
    lblGameState: TLabel;
    sbRestart: TSpeedButton;
    sbSettings: TSpeedButton;
    sbFlag: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject; Canvas: TCanvas);
    procedure PaintBox1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure PaintBox1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure sbFlagClick(Sender: TObject);
    procedure sbRestartClick(Sender: TObject);
    procedure sbSettingsClick(Sender: TObject);
  private
    { Private declarations }
    mse : TMineSweeperEngine;
    GameSettings : TGameSettings;
    ProcessingClick : boolean;
    ButtonsDown : set of TMouseButton;
    procedure InitialiseBoard;
    function CellSize: integer;
    procedure DrawCell(x, y: integer; r: TRectF; c: TCanvas);
    procedure UpdateScreen;
    function Cell(x, y: integer): TMineSweeperCell;
  public
    { Public declarations }
  end;

var
  frmGame: TfrmGame;

implementation

uses
  Math,
  MinesweeperAudioManagerSound,
  MineSweeperFMSettings;

{$R *.fmx}

procedure TfrmGame.FormCreate(Sender: TObject);
begin
  mse := TMineSweeperEngine.Create;
  mse.Sound := TMineSweeperAudioManagerSound.Create;
  GameSettings := TGameSettings.Create;;
  GameSettings.Width := 9;
  GameSettings.height := 9;
  GameSettings.mines := 10;
  InitialiseBoard;
end;

procedure TfrmGame.FormDestroy(Sender: TObject);
begin
  mse.Free;
  GameSettings.Free;
end;

procedure TfrmGame.InitialiseBoard;
begin
  mse.InitialiseBoard(GameSettings.Width, GameSettings.height, GameSettings.mines);
  UpdateScreen;
end;

procedure TfrmGame.PaintBox1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  ButtonsDown := ButtonsDown + [Button];
end;

procedure TfrmGame.PaintBox1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var
  cellx, celly : integer;
begin
  cellx := Floor(x / CellSize);
  celly := Floor(y / CellSize);

  ButtonsDown := ButtonsDown - [Button];

  if mse.GameState <> TGameState.Playing then
    Exit;

  if ((Button = TMouseButton.mbLeft) and (ButtonsDown = [TMouseButton.mbRight])) or ((Button = TMouseButton.mbRight) and (ButtonsDown = [TMouseButton.mbLeft])) then
  begin
    mse.MiddleClick(cellx, celly);
    ProcessingClick := True;
  end;

  if not ProcessingClick then
  begin
    if Button = TMouseButton.mbLeft then
    begin
      if cell(cellx,celly).Visible then
        Button := TMouseButton.mbMiddle
      else
      if sbFlag.IsPressed then
      begin
        Button := TMouseButton.mbRight;
      end;
    end;


    case Button of
      TMouseButton.mbLeft: mse.LeftClick(cellx, celly);
      TMouseButton.mbRight: mse.RightClick(cellx, celly);
      TMouseButton.mbMiddle: mse.MiddleClick(cellx, celly);
    end;
  end else
    ProcessingClick := ButtonsDown <> [];

  UpdateScreen;
end;


procedure TfrmGame.PaintBox1Paint(Sender: TObject; Canvas: TCanvas);
var
  c : TCanvas;
  y: Integer;
  x: Integer;
  r : TRectF;
begin
  c := PaintBox1.Canvas;
  c.Font.Size := CellSize div 2;
  c.Stroke.Kind := TBrushKind.Solid;
  c.Fill.Kind := TBrushKind.Solid;
  c.BeginScene;
  //draw cells
  for x := 0 to mse.Width-1 do
  begin
    for y := 0 to mse.Height-1 do
    begin
      r := Rect(x*CellSize+1, y*CellSize+1, (x+1)*CellSize, (y+1)*CellSize);
      DrawCell(x, y, r, c);
    end;
  end;
  c.EndScene;
end;

procedure TfrmGame.sbFlagClick(Sender: TObject);
begin
//  sbFlag.IsPressed := not sbFlag.IsPressed;
end;

procedure TfrmGame.sbRestartClick(Sender: TObject);
begin
  InitialiseBoard;
end;

procedure TfrmGame.sbSettingsClick(Sender: TObject);
begin
  TfSettingsFM.GetGameSettings(GameSettings, InitialiseBoard);
end;

function TfrmGame.CellSize: integer;
begin
  result := Round(min(PaintBox1.Width / mse.Width, PaintBox1.Height / mse.Height));
end;

procedure TfrmGame.DrawCell(x, y: integer; r: TRectF; c: TCanvas);
var
  Cell : TMineSweeperCell;
  s : string;
begin
  Cell := mse.Board[x,y];

  if Cell.Flag then
  begin
    c.Fill.Color := TAlphaColorRec.Aqua;
    c.Stroke.Color := TAlphaColorRec.Blue;
  end
  else if Cell.Visible then
  begin
    c.Fill.Color := TAlphaColorRec.LtGray;
    c.Stroke.Color := TAlphaColorRec.Gray;
  end
  else
  begin
    c.Fill.Color := TAlphaColorRec.White;
    c.Stroke.Color := TAlphaColorRec.Gray;
  end;

  c.FillRect(r, 0, 0, AllCorners, 1);

  if Cell.Visible and Cell.Mine then
  begin
    var b := dmMineSweeper.Images.Bitmap(TSizeF.Create(r.Width, r.Height), 1);
    c.DrawBitmap(b, Rect(0,0, b.Width, b.Height), r, 1);
  end else
  if (Cell.NeighbourCount > 0) and Cell.Visible then
  begin
    c.Fill.Color := TAlphaColorRec.Black;
    s := Cell.NeighbourCount.ToString;
    c.FillText(r, s, false, 1, [TFillTextFlag.RightToLeft], TTextAlign.Center, TTextAlign.Center);
  end else
  if Cell.Flag then
  begin
    var b := dmMineSweeper.Images.Bitmap(TSizeF.Create(r.Width, r.Height), 0);
    c.DrawBitmap(b, Rect(0,0, b.Width, b.Height), r, 1);
//    b.Free;
  end;
end;

procedure TfrmGame.UpdateScreen;
  procedure UpdateCaption(Caption : string; Color : TAlphaColor);
  begin
    lblGameState.Text := Caption;
    lblGameState.TextSettings.FontColor := Color;
  end;
begin
  PaintBox1.Repaint;
  case mse.GameState of
    Won:     UpdateCaption('You Won!', TAlphaColorRec.Green);
    Lost:    UpdateCaption('You Lost!', TAlphaColorRec.Red);
    Playing: UpdateCaption('Playing', TAlphaColorRec.Black);
  end;
  if mse.GameState in [Won, Lost] then
    ProcessingClick := False;
end;

function TfrmGame.Cell(x, y: integer): TMineSweeperCell;
begin
  result := mse.Board[x, y];
end;

end.
