unit MineSweeperFMSettings;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, MineSweeperSettings, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit, FMX.EditBox, FMX.SpinBox, FMX.Layouts;

type
  TfSettingsFM = class(TForm)
    grpDifficulty: TGroupBox;
    rbEasy: TRadioButton;
    rbMedium: TRadioButton;
    rbExpert: TRadioButton;
    rbCustom: TRadioButton;
    tlbTop: TToolBar;
    sbBack: TSpeedButton;
    sbApply: TSpeedButton;
    layoutCustom: TLayout;
    sbWidth: TSpinBox;
    sbHeight: TSpinBox;
    sbMines: TSpinBox;
    lWidth: TLabel;
    lHeight: TLabel;
    lMines: TLabel;
    procedure OnDifficultyChange(Sender: TObject);
    procedure sbBackClick(Sender: TObject);
    procedure sbApplyClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    GameSettings : TGameSettings;
    OnChangeApplied : TProc;
    procedure UpdateScreen;
    procedure DetectDefaults;
    function GetBoardWidth: Integer;
    procedure SetBoardWidth(const Value: Integer);
    function GetBoardHeight: Integer;
    procedure SetBoardHeight(const Value: Integer);
    function GetMineCount: Integer;
    procedure SetMineCount(const Value: Integer);
    property BoardWidth: Integer read GetBoardWidth write SetBoardWidth;
    property BoardHeight: Integer read GetBoardHeight write SetBoardHeight;
    property MineCount: Integer read GetMineCount write SetMineCount;
  public
    { Public declarations }
    class procedure GetGameSettings(gs : TGameSettings; OnChangeApplied : TProc);
  end;

implementation

{$R *.fmx}

{ TfSettingsFM }

procedure TfSettingsFM.DetectDefaults;
var
  Difficulty : TGameDifficulty;
begin
  Difficulty := TGameSettings.GetDifficulty(BoardWidth, BoardHeight, MineCount);
  case Difficulty of
    Easy: rbEasy.IsChecked := True;
    Medium: rbMedium.IsChecked := True;
    Expert: rbExpert.IsChecked := True;
    Custom: rbCustom.IsChecked := True;
  end;
  UpdateScreen;
end;

procedure TfSettingsFM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := TCloseAction.caFree;
end;

procedure TfSettingsFM.FormShow(Sender: TObject);
begin
  UpdateScreen;
end;

function TfSettingsFM.GetBoardHeight: Integer;
begin
  result := Round(sbHeight.Value);
end;

function TfSettingsFM.GetBoardWidth: Integer;
begin
  result := round(sbWidth.Value);
end;

class procedure TfSettingsFM.GetGameSettings(gs : TGameSettings; OnChangeApplied : TProc);
var
  f : TfSettingsFM;
begin
  f := TfSettingsFM.Create(nil);
  f.OnChangeApplied := OnChangeApplied;
  f.GameSettings := gs;
  f.BoardWidth := gs.Width;
  f.BoardHeight := gs.height;
  f.MineCount := gs.mines;
  f.DetectDefaults;
  f.Show;
end;

function TfSettingsFM.GetMineCount: Integer;
begin
  result := round(sbMines.Value);
end;

procedure TfSettingsFM.OnDifficultyChange(Sender: TObject);
var
 rb : TRadioButton;
 gs : TGameSettings;
begin
  gs := TGameSettings.Create;
  rb := (Sender as TRadioButton);
  if rb.IsChecked then
  begin
    if rb = rbEasy then
      gs.SetDifficulty(TGameDifficulty.Easy)
    else if rb = rbMedium then
      gs.SetDifficulty(TGameDifficulty.Medium)
    else if rb = rbExpert then
      gs.SetDifficulty(TGameDifficulty.Expert)
    else
      Exit;
  end;
  BoardWidth := gs.Width;
  BoardHeight := gs.Height;
  MineCount := gs.Mines;
  gs.Free;
  UpdateScreen;
end;

procedure TfSettingsFM.sbApplyClick(Sender: TObject);
begin
  GameSettings.Width := BoardWidth;
  GameSettings.height := BoardHeight;
  GameSettings.mines := MineCount;
  OnChangeApplied();
  Close;
end;

procedure TfSettingsFM.sbBackClick(Sender: TObject);
begin
  Close;
end;

procedure TfSettingsFM.SetBoardHeight(const Value: Integer);
begin
  sbHeight.Value := Value;
end;

procedure TfSettingsFM.SetBoardWidth(const Value: Integer);
begin
  sbWidth.Value := Value;
end;

procedure TfSettingsFM.SetMineCount(const Value: Integer);
begin
  sbMines.Value := Value;
end;

procedure TfSettingsFM.UpdateScreen;
begin
  layoutCustom.Visible := rbCustom.IsChecked;
end;

end.
