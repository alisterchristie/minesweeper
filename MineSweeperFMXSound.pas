unit MineSweeperFMXSound;

interface

uses
  MinesweeperSoundInterface,
  FMX.Media;

type
  TMineSweeperFMXSound = class(TInterfacedObject, IMinesweeperSound)
  protected
    mp : TMediaPlayer;
    procedure Play(Name : string);
    procedure Win;
    procedure Flag;
    procedure Click;
    procedure Error;
    procedure Mine;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TMineSweeperFMXSound }

procedure TMineSweeperFMXSound.Click;
begin
  Play('Click');
end;

constructor TMineSweeperFMXSound.Create;
begin
  inherited;
  mp := TMediaPlayer.Create(nil);
end;

destructor TMineSweeperFMXSound.Destroy;
begin
  mp.Free;
  inherited;
end;

procedure TMineSweeperFMXSound.Error;
begin

end;

procedure TMineSweeperFMXSound.Flag;
begin
  Play('Flag');
end;

procedure TMineSweeperFMXSound.Mine;
begin
  Play('Mine');
end;

procedure TMineSweeperFMXSound.Play(Name: string);
begin
  mp.FileName := 'C:\Users\Alister\Documents\RAD Studio\Projects\Minesweeper\Sounds\' + Name + '.mp3';
  mp.Play;
end;

procedure TMineSweeperFMXSound.Win;
begin
  Play('Win');
end;

end.
