program MineSweeperHex;

uses
  System.StartUpCopy,
  FMX.Forms,
  GridForm in 'GridForm.pas' {Form23},
  MineSweeperEngine in 'MineSweeperEngine.pas',
  MineSweeperNeighbourCalculator in 'MineSweeperNeighbourCalculator.pas',
  MinesweeperAudioManagerSound in 'MinesweeperAudioManagerSound.pas',
  AudioManager in 'AudioManager.pas',
  MineSweeperFMSettings in 'MineSweeperFMSettings.pas' {fSettingsFM},
  MineSweeperSettings in 'MineSweeperSettings.pas',
  MineSweeperDataModuleFMX in 'MineSweeperDataModuleFMX.pas' {dmMineSweeper: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm23, Form23);
  Application.CreateForm(TdmMineSweeper, dmMineSweeper);
  Application.CreateForm(TdmMineSweeper, dmMineSweeper);
  Application.Run;
end.
