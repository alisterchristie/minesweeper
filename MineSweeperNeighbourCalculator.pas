unit MineSweeperNeighbourCalculator;

interface

type
  TCellRef = record
    x, y : integer;
    function init(x, y : integer) : TCellRef;
    constructor Create(x, y : integer);
  end;

  TCellRefs = TArray<TCellref>;

  INeighbourCalculator = interface
    function GetNeighbours(x,y : integer) : TCellRefs;
    function ValidCell(x, y : integer) : boolean;
  end;

  TGridNeighbourCalculator = class(TInterfacedObject, INeighbourCalculator)
  protected
    ColCount, RowCount : integer;
  public
    constructor Create(ColCount, RowCount : integer);
    function ValidCell(x, y : integer) : boolean; virtual;
    function GetNeighbours(x,y : integer) : TCellRefs; virtual; abstract;

  end;

  TSquareNeighbourCalculator = class(TGridNeighbourCalculator)
  public
    function GetNeighbours(x,y : integer) : TCellRefs; override;
  end;

  THexagonNeighbourCalculator = class(TGridNeighbourCalculator)
  public
    function GetNeighbours(x,y : integer) : TCellRefs; override;
  end;

implementation

{ TNeighbourCalculator }
uses
  System.Generics.Collections;

constructor TGridNeighbourCalculator.Create(ColCount, RowCount : integer);
begin
  self.ColCount := ColCount;
  self.RowCount := RowCount;
end;

function TSquareNeighbourCalculator.GetNeighbours(x, y: integer): TCellRefs;
var
  l : TList<TCellRef>;
  c : TCellRef;
begin
  l := TList<TCellRef>.Create;
  //Top Row
  if ValidCell(x-1, y-1) then
    l.Add(c.init(x-1, y-1));

  if ValidCell(x-1, y) then
    l.Add(c.init(x-1, y));

  if ValidCell(x-1, y+1) then
    l.Add(c.init(x-1, y+1));

  //Left and Right
  if ValidCell(x, y-1) then
    l.Add(c.init(x, y-1));

  if ValidCell(x, y+1) then
    l.Add(c.init(x, y+1));

  //Bottom Row
  if ValidCell(x+1, y-1) then
    l.Add(c.init(x+1, y-1));

  if ValidCell(x+1, y) then
    l.Add(c.init(x+1, y));

  if ValidCell(x+1, y+1) then
    l.Add(c.init(x+1, y+1));

  result := l.ToArray;
  l.Free;
end;

function TGridNeighbourCalculator.ValidCell(x, y: integer): boolean;
begin
  result := (0 <= x) and (x < ColCount) and (0 <= y) and (y < RowCount);
end;

{ TCellRef }

constructor TCellRef.Create(x, y: integer);
begin
  self.x := x;
  self.y := y;
end;

function TCellRef.init(x, y : integer) : TCellRef;
begin
  self.x := x;
  self.y := y;
  result := self;
end;

{ THexagonNeighbourCalculator }

function THexagonNeighbourCalculator.GetNeighbours(x, y: integer): TCellRefs;
var
  l : TList<TCellRef>;
  c : TCellRef;
begin
  l := TList<TCellRef>.Create;

  //clockwise from cell above (I think)
  if ValidCell(x, y-1) then
    l.Add(c.init(x, y-1)); //top

  if ValidCell(x, y+1) then
    l.Add(c.init(x, y+1)); //bottom

  if (x mod 2) = 0 then
  begin //even row
    if ValidCell(x+1, y-1) then
      l.Add(c.init(x+1, y-1)); //top right

    if ValidCell(x+1, y) then
      l.Add(c.init(x+1, y)); //bottom right

    if ValidCell(x-1, y) then
      l.Add(c.init(x-1, y)); //bottom left

    if ValidCell(x-1, y-1) then
      l.Add(c.init(x-1, y-1)); //top left
  end else
  begin //odd row
    if ValidCell(x+1, y) then
      l.Add(c.init(x+1, y)); //top right

    if ValidCell(x+1, y+1) then
      l.Add(c.init(x+1, y+1)); //bottom right

    if ValidCell(x-1, y+1) then
      l.Add(c.init(x-1, y+1)); //bottom left

    if ValidCell(x-1, y) then
      l.Add(c.init(x-1, y)); //top left
  end;

  result := l.ToArray;
  l.Free;
end;

end.
