unit MineSweeperSettings;

interface

type
  TGameDifficulty = (Easy, Medium, Expert, Custom);
  TGameSettings = class
  public
    Width, Height, Mines : integer;
    procedure SetDifficulty(Difficulty : TGameDifficulty);
    procedure SetValues(Width, Height, Mines: integer);
    function GetDifficulty : TGameDifficulty; overload;
    class function GetDifficulty(Width, Height, Mines : integer) : TGameDifficulty; overload;
  end;

implementation

{ TGameSettings }

function TGameSettings.GetDifficulty: TGameDifficulty;
begin
  if (Width = 9) and (Height = 9) and (Mines = 10) then
    result := TGameDifficulty.Easy
  else if (Width = 16) and (Height = 16) and (Mines = 40) then
    result := TGameDifficulty.Medium
  else if (Width = 16) and (Height = 30) and (Mines = 99) then
    result := TGameDifficulty.Expert
  else
    result := TGameDifficulty.Custom;
end;

class function TGameSettings.GetDifficulty(Width, Height, Mines: integer): TGameDifficulty;
var
  gs : TGameSettings;
begin
  gs := TGameSettings.Create;
  gs.SetValues(Width, Height, Mines);
  result := gs.GetDifficulty;
  gs.Free;
end;

procedure TGameSettings.SetDifficulty(Difficulty: TGameDifficulty);
begin
  case Difficulty of
    Easy: SetValues(9, 9, 10);
    Medium: SetValues(16, 16, 40);
    Expert: SetValues(16, 30, 99);
    Custom: ; //do nothing
  end;
end;

procedure TGameSettings.SetValues(Width, Height, Mines: integer);
begin
  self.Width := Width;
  self.height := Height;
  self.mines := Mines;
end;

end.
