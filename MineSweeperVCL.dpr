program MineSweeperVCL;



uses
  Vcl.Forms,
  MineSweeperVCLForm in 'MineSweeperVCLForm.pas' {frmGame},
  MineSweeperEngine in 'MineSweeperEngine.pas',
  MinesweeperWindowSound in 'MinesweeperWindowSound.pas',
  MineSweeperVCLSettings in 'MineSweeperVCLSettings.pas' {frmSettings},
  MineSweeperDataModule in 'MineSweeperDataModule.pas' {dmMineSweeper: TDataModule},
  MinesweeperSoundInterface in 'MinesweeperSoundInterface.pas',
  MineSweeperNeighbourCalculator in 'MineSweeperNeighbourCalculator.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmMineSweeper, dmMineSweeper);
  Application.CreateForm(TfrmGame, frmGame);
  Application.Run;
end.
