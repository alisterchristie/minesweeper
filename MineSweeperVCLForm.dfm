object frmGame: TfrmGame
  Left = 0
  Top = 0
  Caption = 'Minesweeper Clone'
  ClientHeight = 581
  ClientWidth = 518
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 380
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    518
    581)
  PixelsPerInch = 96
  TextHeight = 13
  object pbMineField: TPaintBox
    Left = 8
    Top = 72
    Width = 501
    Height = 501
    Anchors = [akLeft, akTop, akRight, akBottom]
    OnMouseDown = pbMineFieldMouseDown
    OnMouseUp = pbMineFieldMouseUp
    OnPaint = pbMineFieldPaint
  end
  object lblGameState: TLabel
    Left = 208
    Top = 13
    Width = 100
    Height = 33
    Anchors = [akTop]
    Caption = 'Playing'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnRestart: TSpeedButton
    Left = 8
    Top = 8
    Width = 57
    Height = 57
    Action = aRestart
    ImageIndex = 1
    ImageName = 'Refresh_48'
    Images = VirtualImageList1
  end
  object btnSettings: TSpeedButton
    Left = 453
    Top = 8
    Width = 57
    Height = 57
    Action = aSettings
    Anchors = [akTop, akRight]
    ImageIndex = 0
    ImageName = 'Cogs_48'
    Images = VirtualImageList1
  end
  object ActionList1: TActionList
    Left = 240
    Top = 328
    object aRestart: TAction
      ImageIndex = 0
      OnExecute = aRestartExecute
    end
    object aSettings: TAction
      ImageIndex = 1
      OnExecute = aSettingsExecute
    end
  end
  object VirtualImageList1: TVirtualImageList
    DisabledGrayscale = False
    DisabledSuffix = '_Disabled'
    Images = <
      item
        CollectionIndex = 2
        CollectionName = 'Cogs_48'
        Disabled = False
        Name = 'Cogs_48'
      end
      item
        CollectionIndex = 3
        CollectionName = 'Refresh_48'
        Disabled = False
        Name = 'Refresh_48'
      end>
    ImageCollection = dmMineSweeper.ImageCollection1
    Width = 48
    Height = 48
    Left = 256
    Top = 280
  end
end
