unit MineSweeperVCLForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, MineSweeperEngine, Vcl.StdCtrls,
  System.Actions, Vcl.ActnList, Vcl.Buttons, MinesweeperWindowSound, System.ImageList, Vcl.ImgList,
  MineSweeperDataModule, MineSweeperVCLSettings, MineSweeperSettings, MinesweeperSoundInterface,
  Vcl.VirtualImageList;

type
  TfrmGame = class(TForm)
    pbMineField: TPaintBox;
    ActionList1: TActionList;
    aRestart: TAction;
    lblGameState: TLabel;
    btnRestart: TSpeedButton;
    btnSettings: TSpeedButton;
    aSettings: TAction;
    VirtualImageList1: TVirtualImageList;
    procedure pbMineFieldPaint(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure pbMineFieldMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure aRestartExecute(Sender: TObject);
    procedure pbMineFieldMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure aSettingsExecute(Sender: TObject);
  private
    mse : TMineSweeperEngine;
    ButtonsDown : set of TMouseButton; //We want to know what buttons are down so we can detect Left+Right click
    ProcessingClick : boolean;
    GameSettings : TGameSettings;
    procedure DrawCell(x, y : integer; r : TRect; c : TCanvas);
    procedure InitialiseBoard;

    function CellSize : integer;
    procedure UpdateScreen;
    function Cell(x, y : integer) : TMineSweeperCell;

  public
    { Public declarations }
  end;

var
  frmGame: TfrmGame;

implementation

uses
  System.Types, Math;

{$R *.dfm}

procedure TfrmGame.aRestartExecute(Sender: TObject);
begin
  InitialiseBoard;
  UpdateScreen;
end;

procedure TfrmGame.aSettingsExecute(Sender: TObject);
begin
  if TfrmSettings.GetGameSettings(GameSettings) then
    InitialiseBoard;
end;

function TfrmGame.Cell(x, y: integer): TMineSweeperCell;
begin
  result := mse.Board[x, y];
end;


function TfrmGame.CellSize: integer;
begin
  result := min(pbMineField.Width div mse.Width, pbMineField.Height div mse.Height);
end;

procedure TfrmGame.DrawCell(x, y: integer; r: TRect; c: TCanvas);
var
  Cell : TMineSweeperCell;
  cx, cy : integer;
  s : string;
begin
  Cell := mse.Board[x,y];

  if Cell.Flag then
  begin
    c.Brush.Color := clAqua;
    c.Pen.Color := clBlue;
  end
  else if Cell.Visible then
  begin
    c.Brush.Color := clLtGray;
    c.Pen.Color := clGray;
  end
  else
  begin
    c.Brush.Color := clWhite;
    c.Pen.Color := clGray;
  end;

  c.Rectangle(r.TopLeft.X, r.TopLeft.Y, r.BottomRight.X, r.BottomRight.Y);

  if Cell.Visible and Cell.Mine then
  begin
    c.Brush.Color := clRed;
    c.Pen.Color := clMaroon;
    c.Ellipse(r.TopLeft.X+2, r.TopLeft.Y+2, r.BottomRight.X-2, r.BottomRight.Y-2);
  end else
  if (Cell.NeighbourCount > 0) and Cell.Visible then
  begin
    cx := CellSize div 2;
    cy := CellSize div 2;
    s := Cell.NeighbourCount.ToString;
    cx := cx - c.TextWidth(s) div 2 - 1;
    cy := cy - c.TextHeight(s) div 2 - 1;
    c.Brush.Style := bsClear;
    c.TextOut(r.TopLeft.X + cx, r.TopLeft.Y + cy, s);
    c.Brush.Style := bsSolid;
  end;
end;

procedure TfrmGame.FormCreate(Sender: TObject);
begin
  mse := TMineSweeperEngine.Create;
  mse.Sound := TMinesweeperWindowsSound.Create;
  GameSettings := TGameSettings.Create;
  GameSettings.Width := 9;
  GameSettings.height := 9;
  GameSettings.mines := 10;
  InitialiseBoard;
  ButtonsDown := [];
  ProcessingClick := False;
end;

procedure TfrmGame.FormDestroy(Sender: TObject);
begin
  mse.Free;
  GameSettings.Free;
end;

procedure TfrmGame.InitialiseBoard;
begin
  mse.InitialiseBoard(GameSettings.Width, GameSettings.height, GameSettings.mines);
  pbMineField.Invalidate;
end;

procedure TfrmGame.pbMineFieldMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ButtonsDown := ButtonsDown + [Button];
end;

procedure TfrmGame.pbMineFieldMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  cellx, celly : integer;
begin
  cellx := x div CellSize;
  celly := y div CellSize;

  ButtonsDown := ButtonsDown - [Button];

  if mse.GameState <> TGameState.Playing then
    Exit;

  if ((Button = mbLeft) and (ButtonsDown = [mbRight])) or ((Button = mbRight) and (ButtonsDown = [mbLeft])) then
  begin
    mse.MiddleClick(cellx, celly);
    ProcessingClick := True;
  end;

  if not ProcessingClick then
  begin
    case Button of
      mbLeft: mse.LeftClick(cellx, celly);
      mbRight: mse.RightClick(cellx, celly);
      mbMiddle: mse.MiddleClick(cellx, celly);
    end;
  end else
    ProcessingClick := ButtonsDown <> [];

  UpdateScreen;
end;

procedure TfrmGame.pbMineFieldPaint(Sender: TObject);
var
  c : TCanvas;
  y: Integer;
  x: Integer;
  r : TRect;

begin
  c := pbMineField.Canvas;
  c.Font.Size := CellSize div 2;

  //draw cells
  for x := 0 to mse.Width-1 do
  begin
    for y := 0 to mse.Height-1 do
    begin
      r := Rect(x*CellSize+1, y*CellSize+1, (x+1)*CellSize, (y+1)*CellSize);
      DrawCell(x, y, r, c);
    end;
  end;
end;


procedure TfrmGame.UpdateScreen;
  procedure UpdateCaption(Caption : string; Color : TColor);
  begin
    lblGameState.Caption := Caption;
    lblGameState.Font.Color := Color;
  end;
begin
  pbMineField.Invalidate;
  case mse.GameState of
    Won:     UpdateCaption('You Won!', clGreen);
    Lost:    UpdateCaption('You Lost!', clRed);
    Playing: UpdateCaption('Playing', clBlack);
  end;
  if mse.GameState in [Won, Lost] then
    ProcessingClick := False;
end;

end.
