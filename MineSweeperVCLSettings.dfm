object frmSettings: TfrmSettings
  Left = 0
  Top = 0
  Caption = 'Settings'
  ClientHeight = 279
  ClientWidth = 204
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnOK: TSpeedButton
    Left = 137
    Top = 215
    Width = 59
    Height = 56
    ImageIndex = 1
    ImageName = 'Tick_48'
    Images = VirtualImageList1
    OnClick = btnOKClick
  end
  object btnCancel: TSpeedButton
    Left = 8
    Top = 217
    Width = 57
    Height = 54
    ImageIndex = 0
    ImageName = 'Cancel_48'
    Images = VirtualImageList1
    OnClick = btnCancelClick
  end
  object rgDifficulty: TRadioGroup
    Left = 8
    Top = 8
    Width = 185
    Height = 145
    Caption = 'Game Difficulty'
    ItemIndex = 0
    Items.Strings = (
      'Easy'
      'Medium'
      'Expert'
      'Custom')
    TabOrder = 0
    OnClick = rgDifficultyClick
  end
  object pCustom: TPanel
    Left = 8
    Top = 159
    Width = 185
    Height = 50
    TabOrder = 1
    object Label1: TLabel
      Left = 10
      Top = 0
      Width = 28
      Height = 13
      Caption = 'Width'
    end
    object Label2: TLabel
      Left = 74
      Top = 0
      Width = 31
      Height = 13
      Caption = 'Height'
    end
    object Label3: TLabel
      Left = 138
      Top = 0
      Width = 27
      Height = 13
      Caption = 'Mines'
    end
    object seWidth: TSpinEdit
      Left = 10
      Top = 19
      Width = 39
      Height = 22
      MaxValue = 99
      MinValue = 5
      TabOrder = 0
      Value = 5
    end
    object seHeight: TSpinEdit
      Left = 74
      Top = 19
      Width = 43
      Height = 22
      MaxValue = 99
      MinValue = 5
      TabOrder = 1
      Value = 5
    end
    object seMines: TSpinEdit
      Left = 138
      Top = 19
      Width = 39
      Height = 22
      MaxValue = 99
      MinValue = 5
      TabOrder = 2
      Value = 5
    end
  end
  object VirtualImageList1: TVirtualImageList
    DisabledGrayscale = False
    DisabledSuffix = '_Disabled'
    Images = <
      item
        CollectionIndex = 0
        CollectionName = 'Cancel_48'
        Disabled = False
        Name = 'Cancel_48'
      end
      item
        CollectionIndex = 1
        CollectionName = 'Tick_48'
        Disabled = False
        Name = 'Tick_48'
      end>
    ImageCollection = dmMineSweeper.ImageCollection1
    Width = 48
    Height = 48
    Left = 120
    Top = 88
  end
end
