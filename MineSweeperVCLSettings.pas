unit MineSweeperVCLSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Samples.Spin, MineSweeperSettings,
  Vcl.Buttons, System.ImageList, Vcl.ImgList, Vcl.VirtualImageList;

type

  TfrmSettings = class(TForm)
    rgDifficulty: TRadioGroup;
    btnOK: TSpeedButton;
    btnCancel: TSpeedButton;
    seWidth: TSpinEdit;
    pCustom: TPanel;
    seHeight: TSpinEdit;
    seMines: TSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    VirtualImageList1: TVirtualImageList;
    procedure rgDifficultyClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    function GetBoardWidth: integer;
    procedure SetBoardWidth(const Value: integer);
    function GetBoardHeight: integer;
    procedure SetBoardHeight(const Value: integer);
    function GetMineCount: integer;
    procedure SetMineCount(const Value: integer);
    procedure DetectDefaults;
    procedure DetermineCustomVisibility;
  public
    { Public declarations }
    property BoardWidth: integer read GetBoardWidth write SetBoardWidth;
    property BoardHeight: integer read GetBoardHeight write SetBoardHeight;
    property MineCount: integer read GetMineCount write SetMineCount;

    class function GetGameSettings(var gs : TGameSettings) : boolean;
  end;

implementation

{$R *.dfm}

uses MineSweeperDataModule;

procedure TfrmSettings.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmSettings.btnOKClick(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TfrmSettings.DetectDefaults;
var
  Difficulty : TGameDifficulty;
  gs : TGameSettings;
begin
  Difficulty := TGameSettings.GetDifficulty(BoardWidth, BoardHeight, MineCount);
  case Difficulty of
    Easy: rgDifficulty.ItemIndex := 0;
    Medium: rgDifficulty.ItemIndex := 1;
    Expert: rgDifficulty.ItemIndex := 2;
    Custom: rgDifficulty.ItemIndex := 3;
  end;
  DetermineCustomVisibility;
end;

procedure TfrmSettings.FormCreate(Sender: TObject);
begin
  pCustom.BevelOuter := bvNone;
end;

function TfrmSettings.GetBoardHeight: integer;
begin
  result := seHeight.Value;
end;

function TfrmSettings.GetBoardWidth: integer;
begin
  result := seWidth.Value;
end;

class function TfrmSettings.GetGameSettings(var gs: TGameSettings): boolean;
var
  f : TfrmSettings;
begin
  f := TfrmSettings.Create(nil);
  try
    f.BoardWidth := gs.Width;
    f.BoardHeight := gs.height;
    f.MineCount := gs.mines;
    f.DetectDefaults;
    result := f.ShowModal = mrOK;
    if result then
    begin
      gs.Width := f.BoardWidth;
      gs.height := f.BoardHeight;
      gs.mines := f.MineCount;
    end;
  finally
    f.Free;
  end;
end;

procedure TfrmSettings.DetermineCustomVisibility;
begin
  //Custom
  pCustom.Visible := rgDifficulty.ItemIndex = 3;
end;

function TfrmSettings.GetMineCount: integer;
begin
  result := seMines.Value;
end;

procedure TfrmSettings.rgDifficultyClick(Sender: TObject);
var
  gs : TGameSettings;
begin
  gs := TGameSettings.Create;
  DetermineCustomVisibility;
  case rgDifficulty.ItemIndex of
    0: gs.SetDifficulty(TGameDifficulty.Easy);
    1: gs.SetDifficulty(TGameDifficulty.Medium);
    2: gs.SetDifficulty(TGameDifficulty.Expert);
    3: Exit; //don't want to change any of the values
  end;

  BoardHeight := gs.height;
  BoardWidth := gs.Width;
  MineCount := gs.Mines;
  gs.Free;
end;

procedure TfrmSettings.SetBoardHeight(const Value: integer);
begin
  seHeight.Value := Value;
end;

procedure TfrmSettings.SetBoardWidth(const Value: integer);
begin
  seWidth.Value := Value;
end;

procedure TfrmSettings.SetMineCount(const Value: integer);
begin
  seMines.Value := Value;
end;

end.
