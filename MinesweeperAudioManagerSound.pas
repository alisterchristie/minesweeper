unit MinesweeperAudioManagerSound;

interface

uses AudioManager, MinesweeperSoundInterface;

type
  TMineSweeperAudioManagerSound = class(TMineSweeperStringSound)
  private
    am : TAudioManager;
    DataFilePath : string;
    procedure InitPaths;
    procedure AddSound(Sound : string);
  protected
    procedure PlaySound(Sound: string); override;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  System.IOUtils,
  System.SysUtils;

{ TMineSweeperAudioManagerSound }

procedure TMineSweeperAudioManagerSound.AddSound(Sound: string);
begin
  am.AddSound(TPath.Combine(DataFilePath, Sound + '.wav'));
end;

constructor TMineSweeperAudioManagerSound.Create;
begin
  inherited;
  InitPaths;
  am := TAudioManager.Create;
  AddSound('Click');
  AddSound('Error');
  AddSound('Flag');
  AddSound('Mine');
  AddSound('Win');
end;

destructor TMineSweeperAudioManagerSound.Destroy;
begin
  am.Free;
  inherited;
end;

procedure TMineSweeperAudioManagerSound.InitPaths;
begin
{$IF DEFINED(MSWINDOWS)}
  DataFilePath := TPath.Combine(ExtractFilePath(ParamStr(0)), 'Sounds');
{$ELSE}
{$IF (DEFINED(MACOS) AND NOT DEFINED(IOS))}
  DataFilePath := TPath.GetHomePath;
{$ELSE}
  DataFilePath := TPath.GetDocumentsPath;
{$ENDIF}
{$ENDIF}
  DataFilePath := IncludeTrailingPathDelimiter(DataFilePath);
end;


procedure TMineSweeperAudioManagerSound.PlaySound(Sound: string);
begin
  inherited;
  am.PlaySound(Sound);
end;

end.
