unit MinesweeperSoundInterface;

interface

type
  IMinesweeperSound = interface
    ['{A648794A-13D3-45E7-B2AB-CC32D75CF4AE}']
    procedure Win;
    procedure Flag;
    procedure Click;
    procedure Error;
    procedure Mine;
  end;

  TMinesweeperDummySound = class(TInterfacedObject, IMinesweeperSound)
    procedure Win;
    procedure Flag;
    procedure Click;
    procedure Error;
    procedure Mine;
  end;

  TMineSweeperStringSound  = class(TInterfacedObject, IMinesweeperSound)
  protected
    procedure PlaySound(Sound : string); virtual; abstract;
    procedure Win;
    procedure Flag;
    procedure Click;
    procedure Error;
    procedure Mine;
  end;


implementation

{ TMinesweeperDummySound }

procedure TMinesweeperDummySound.Click;
begin

end;

procedure TMinesweeperDummySound.Error;
begin

end;

procedure TMinesweeperDummySound.Flag;
begin

end;

procedure TMinesweeperDummySound.Mine;
begin

end;

procedure TMinesweeperDummySound.Win;
begin

end;

{ TMineSweeperStringSound }

procedure TMineSweeperStringSound.Click;
begin
  PlaySound('Click');
end;

procedure TMineSweeperStringSound.Error;
begin
  PlaySound('Error');
end;

procedure TMineSweeperStringSound.Flag;
begin
  PlaySound('Flag');
end;

procedure TMineSweeperStringSound.Mine;
begin
  PlaySound('Mine');
end;

procedure TMineSweeperStringSound.Win;
begin
  PlaySound('Win');
end;

end.
