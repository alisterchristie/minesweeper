unit MinesweeperWindowSound;

interface

uses
  MinesweeperSoundInterface;

{$R Sounds.dres}

type

  TMinesweeperWindowsSound = class(TInterfacedObject, IMinesweeperSound)
  private
    procedure Play(ResourceName : string);
  public
    procedure Win;
    procedure Flag;
    procedure Click;
    procedure Error;
    procedure Mine;
  end;

implementation

uses
  MMSystem;

{ TMinesweeperWindowsSound }

procedure TMinesweeperWindowsSound.Click;
begin
  Play('Sound_Click');
end;

procedure TMinesweeperWindowsSound.Error;
begin
  Play('Sound_Error');
end;

procedure TMinesweeperWindowsSound.Flag;
begin
  Play('Sound_Flag');
end;

procedure TMinesweeperWindowsSound.Mine;
begin
  Play('Sound_Mine');
end;

procedure TMinesweeperWindowsSound.Play(ResourceName: string);
begin
  PlaySound(PChar(ResourceName), 0, SND_RESOURCE or SND_ASYNC);
end;

procedure TMinesweeperWindowsSound.Win;
begin
  Play('Sound_Win');
end;

end.
