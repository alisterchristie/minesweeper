unit MineSweeperEngineTests;

interface

uses
  DUnitX.TestFramework, MineSweeperEngine;

type
  TMineSweeperEngineTester = class(TMineSweeperEngine); //access protected methods

  [TestFixture]
  TestMineSweeperEngine = class
  protected
    m : TMineSweeperEngineTester;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    // Sample Methods
    // Simple single Test
    [Test]
    procedure InitializeBoardAndPlaceMines;
    [Test]
    procedure WinSimpleGame;
    [Test]
    procedure LoseSimpleGame;
    [Test]
    procedure TestNeighbourCount;
    [Test]
    procedure TestNeighbourMineCount;
    [Test]
    procedure TotalMineCount;
    [Test, Ignore]
    procedure FirstMoveCannotLoose;

    // Test with TestCase Attribute to supply parameters.
//    [Test]
//    [TestCase('TestA','1,2')]
//    [TestCase('TestB','3,4')]
    procedure Test2(const AValue1 : Integer;const AValue2 : Integer);
  end;

implementation

procedure TestMineSweeperEngine.LoseSimpleGame;
begin
  m.InitialiseBoard(2,2,0);
  m.Board[0,0].Mine := true;

  m.Show(0,1,true);
  Assert.AreEqual(TGameState.Playing, m.GameState);
  m.Show(1,0,true);
  Assert.AreEqual(TGameState.Playing, m.GameState);
  m.Show(1,1,true);
  Assert.AreEqual(TGameState.Playing, m.GameState);
  m.Show(0,0, true);
  Assert.AreEqual(TGameState.Lost, m.GameState);
end;

procedure TestMineSweeperEngine.Setup;
begin
  m := TMineSweeperEngineTester.Create;
end;

procedure TestMineSweeperEngine.TearDown;
begin
  m.Free;
end;

procedure TestMineSweeperEngine.FirstMoveCannotLoose;
begin
  //need to work out how to fix this test
  m.InitialiseBoard(2,2,0);
  m.Board[0,0].Mine := True;
  m.Show(0,0, True);
//  m.LeftClick(0, 0);
  Assert.AreEqual(TGameState.Playing, m.GameState);
end;

procedure TestMineSweeperEngine.InitializeBoardAndPlaceMines;
begin
  m.InitialiseBoard(5,5,0);
  m.PlaceMines(5);
  var MineCount : integer := 0;
  for var i := 0 to 4 do
  begin
    for var j := 0 to 4 do
    begin
      if m.Board[i,j].Mine then
        inc(MineCount);
    end;
  end;
  Assert.AreEqual(5, MineCount);
end;

procedure TestMineSweeperEngine.Test2(const AValue1 : Integer;const AValue2 : Integer);
begin
end;

procedure TestMineSweeperEngine.TestNeighbourCount;
begin
  m.InitialiseBoard(5,5, 0);
  m.CalculateAllNeighbours;

  var n := m.GetNeighbours(0,0);
  Assert.AreEqual(3, Length(n), 'Top Left');

  n := m.GetNeighbours(1, 0);
  Assert.AreEqual(5, Length(n), 'Top edge');

  n := m.GetNeighbours(4, 0);
  Assert.AreEqual(3, Length(n), 'Top right');

  n := m.GetNeighbours(0, 2);
  Assert.AreEqual(5, Length(n), 'Left edge');

  n := m.GetNeighbours(4, 3);
  Assert.AreEqual(5, Length(n), 'Right edge');

  n := m.GetNeighbours(0, 4);
  Assert.AreEqual(3, Length(n), 'Bottom left');

  n := m.GetNeighbours(3, 4);
  Assert.AreEqual(5, Length(n), 'Bottom middle');

  n := m.GetNeighbours(4, 4);
  Assert.AreEqual(3, Length(n), 'Bottom right');
end;

procedure TestMineSweeperEngine.TestNeighbourMineCount;
//x 1 0 0
//3 4 2 1
//x x x 2
//2 4 x 2
begin
  m.InitialiseBoard(4,4,0);
  m.Board[0,0].Mine := True;
  m.Board[0,2].Mine := True;
  m.Board[1,2].Mine := True;
  m.Board[2,2].Mine := True;
  m.Board[2,3].Mine := True;
  m.CalculateAllNeighbours;

  Assert.AreEqual(1, m.Board[1,0].NeighbourCount);
  Assert.AreEqual(0, m.Board[2,0].NeighbourCount);
  Assert.AreEqual(3, m.Board[0,1].NeighbourCount);
  Assert.AreEqual(4, m.Board[1,1].NeighbourCount);
  Assert.AreEqual(2, m.Board[2,1].NeighbourCount);
  Assert.AreEqual(2, m.Board[3,3].NeighbourCount);
end;

procedure TestMineSweeperEngine.TotalMineCount;
var
  Count : integer;
begin
  m.InitialiseBoard(5,5,10);
  m.PlaceMines(10);
  Count := 0;

  m.ForEveryCell(
    procedure(var Cell : TMineSweeperCell)
    begin
      if Cell.Mine then
       inc(Count);
    end
  );

  Assert.AreEqual(10, Count);
  Assert.AreEqual(10, m.TotalMineCount);
end;

procedure TestMineSweeperEngine.WinSimpleGame;
begin
  m.InitialiseBoard(2,2,0);
  m.Board[0,0].Mine := true;

  m.SetFlag(0,0);
  Assert.AreEqual(TGameState.Playing, m.GameState);
  m.Show(0,1,true);
  Assert.AreEqual(TGameState.Playing, m.GameState);
  m.Show(1,0,true);
  Assert.AreEqual(TGameState.Playing, m.GameState);
  m.Show(1,1,true);
  Assert.AreEqual(TGameState.Won, m.GameState);
end;

initialization
  TDUnitX.RegisterTestFixture(TestMineSweeperEngine);

end.
