unit NeighbourCalculatorTest;

interface

uses
  DUnitX.TestFramework,
  MineSweeperNeighbourCalculator;

type
  [TestFixture]
  TestSquareNeighbourCalculator = class
  public
    [Test]
    procedure SingleSquareHasNoNeighbours;
    [Test]
    procedure TwoByOne;
  end;

  [TestFixture]
  HexNeighbourCalculator = class
  public
    [Test]
    procedure TwoByTwo;
    [Test]
    procedure FiveByFive;
  end;

implementation

{ TestSquareNeighbourCalculator }

procedure TestSquareNeighbourCalculator.SingleSquareHasNoNeighbours;
begin
  var nc := TSquareNeighbourCalculator.Create(1,1);
  var n := nc.GetNeighbours(0,0);
  Assert.AreEqual(0, length(n));

  Assert.IsTrue(nc.ValidCell(0,0));
  Assert.IsFalse(nc.ValidCell(1,1));
  Assert.IsFalse(nc.ValidCell(0,-1));
  Assert.IsFalse(nc.ValidCell(-1,0));
  nc.Free;
end;

procedure TestSquareNeighbourCalculator.TwoByOne;
begin
  var nc := TSquareNeighbourCalculator.Create(2,1); //2 col, 1 row
  var n := nc.GetNeighbours(0,0);
  Assert.AreEqual(1, length(n), 'Length expected to be 1');
  Assert.AreEqual(1, n[0].x); //only neighbour is column 1, row 0
  Assert.AreEqual(0, n[0].y);
  nc.Free;
end;

{ HexNeighbourCalculator }

procedure HexNeighbourCalculator.FiveByFive;
begin
  var nc : INeighbourCalculator := THexagonNeighbourCalculator.Create(5,5);
  var n := nc.GetNeighbours(0,0);
  Assert.AreEqual(2, Length(n));
  n := nc.GetNeighbours(1,0);
  Assert.AreEqual(5, Length(n));
  n := nc.GetNeighbours(1,1);
  Assert.AreEqual(6, Length(n));

  Assert.IsTrue(nc.ValidCell(4,4));
  Assert.IsFalse(nc.ValidCell(0,5));
  Assert.IsFalse(nc.ValidCell(5,0));
  //should also check what these neighbours are
end;

procedure HexNeighbourCalculator.TwoByTwo;
begin
  var nc := THexagonNeighbourCalculator.Create(2,2);
  var n := nc.GetNeighbours(0,0);
  Assert.AreEqual(2, Length(n));
  nc.Free;
end;

initialization
  TDUnitX.RegisterTestFixture(TestSquareNeighbourCalculator);
  TDUnitX.RegisterTestFixture(HexNeighbourCalculator);

end.
